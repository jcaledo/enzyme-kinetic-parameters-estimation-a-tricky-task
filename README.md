# README #

The file script.Rmd is a R markdown document containing three R functions, lb(), ecb() and dir.MM(),to estimate Km and Vm from kinetic data. The document also provides examples to illustrate the use of these functions.